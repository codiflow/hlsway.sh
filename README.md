# hlsway.sh

Little script to get the webserver name and version string (if available) for lots of websites in no time.  
Or in other words: `H`ey `l`ittle `s`erver, `w`ho `a`re `y`ou?



## Story

One single day I just browsed the web and wanted to know what's the easiest way to get the name of the webserver serving the page I'm currently browing. After trying several methods like putting some strange characters somewhere inside the URL or exceding the maximum length I found the easiest way to get (almost) every webserver to "drop his trousers" (probably this phraseology is totally wrong in english) is as follows:

Just enter two `%%`` somewhere **after** the domain extension and the first slash like in this example:

`https://codiflow.de/%%`

If the webservers error pages are not customized he will probably show you something similar to this:

![Screenshot of a nginx HTTP 400 error message with version string hidden](images/screenshot_error_400_nginx_version_hidden.png)

Here the version string is correctly hidden from the public.

Here is another example with the version string exposed:

![Screenshot of a nginx HTTP 400 error message with version string exposed](images/screenshot_error_400_nginx_version_exposed.png)

For Apache webservers the error message looks like this:

![Screenshot of a Apache HTTP 400 error message with version string exposed](images/screenshot_error_400_apache_version_exposed.png)

As this made me a lot of fun I looked for an easier way to do several requests at once instead from loading them via my browser. So I created this script...



## Usage of the script
Just edit the array `SITES` and enter the urls you want to test. Please notice that every url needs to be surrounded by this little things here: `""`



Afterwards save the file and execute it like you would do with every other shell script: `./hlsway.sh`



More config options and a help will probably be added in the future.



## How does it work

There's an array which holds all domains you want to check. It's **important** that these urls **don't** have any prefixes like `www.`, `http://` or `https://` as they are automatically added by the script. So just enter the plain urls like `codiflow.de` or `subdomain.codiflow.de`.

The script creates **four request urls** for each entry and opens them with `curl`. Heres an example:



Example URL: `codiflow.de`

Request 1: `http://codiflow.de/%%`
Request 2: `http://www.codiflow.de/%%`
Request 3: `https://codiflow.de/%%`
Request 4: `https://www.codiflow.de/`




## HTTP ERROR 400 Showcase

Here I list some of the most common error pages for HTTP ERROR 400

![Screenshot of a nginx HTTP 400 error message with version string hidden](images/screenshot_error_400_nginx_version_hidden.png)
